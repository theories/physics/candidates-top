# theories/physics/candidates-top

## About this project

If you want to add new candidate as theory of everything, this is the right place.

Add new **issue** with label *candidate* (title formated as **name of theory [candidate]**). The maintainer will create a separated repository for your theory. See [nokton theory](https://gitlab.com/theories/physics/candidates/nokton) as example.

You can find the complete list of projects in group **candidates** [here](https://gitlab.com/theories/physics/candidates).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Licenses

Any project content in this project is under [**Creative Commons LICENSE**](LICENSE.txt) and any project code content in this project is under [**MIT LICENSE**](LICENSE-CODE.txt).